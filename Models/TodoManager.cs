 using System.Collections.Generic;  
using System.Linq;
 
namespace MyWebApp.Models
{
	
	public class TodoManager{
		
		public List<Todo> todos = new List<Todo> {
			new Todo {description = "study mvc" },
			new Todo {description = "create my first app" },
			new Todo {description = "add on github" },
			new Todo {description = "thank Allah" },
			new Todo {description = "brag to my friends" },
			
		};
		
		public IEnumerable<Todo> GetAll { get { return todos; } }
	}
}