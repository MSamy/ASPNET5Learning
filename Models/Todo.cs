namespace MyWebApp.Models
{
 public class Todo
{
	static int todocount = 0;
	
	public Todo()
	{
		todocount++;
		id = todocount;
	}
	
	public Todo(string description){
		this.description = description;
	}
	public string description{get; set;}
	public int id {get; set;}
	
	public bool isCompleted { get; set; }
	
	public virtual string ApplicationUserId {get; set;}
}

}